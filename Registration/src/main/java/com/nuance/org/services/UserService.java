package com.nuance.org.services;

import com.nuance.org.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}