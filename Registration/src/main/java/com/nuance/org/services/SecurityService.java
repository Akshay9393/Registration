package com.nuance.org.services;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
