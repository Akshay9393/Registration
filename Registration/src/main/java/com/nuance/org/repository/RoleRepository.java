package com.nuance.org.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nuance.org.model.Role;

public interface RoleRepository extends JpaRepository<Role,Long> {
	

}
